CXX=g++ -fpermissive
CXXFLAGS = -ggdb -Iinclude $(shell pkg-config --cflags bullet)
LDLIBS = $(shell pkg-config --libs bullet) -lglut -lGLU -lGL

GLTargets=DemoApplication.o GLDebugDrawer.o \
										GlutDemoApplication.o GlutStuff.o GL_ShapeDrawer.o \
										GLDebugFont.o VehicleDemo.o

all: main

main: $(GLTargets)

clean:
	rm -f $(GLTargets)
